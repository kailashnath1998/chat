const express = require('express')
const app = express()


//set the template engine ejs
app.set('view engine', 'ejs')

//middlewares
app.use(express.static('public'))


//routes
app.get('/', (req, res) => {
    res.render('index')
})

//Listen on port 3000
server = app.listen(3000)


var users = {

};

//socket.io instantiation
const io = require("socket.io")(server)


//listen on every connection
io.on('connection', (socket) => {
    console.log('New user connected')

    //default username
    socket.username = "Anonymous"

    //listen on change_username
    socket.on('change_username', (data) => {
        console.log(socket.username + "->" + data.username + " : " + socket.id + " : " + socket.connected);

        users[data.username] = socket;
        socket.username = data.username;

    })

    //listen on new_message
    socket.on('new_message', (data) => {
        var reciver = data.message.split(';')[0];
        var message = data.message.split(';')[1];
        console.log('New Message : ' + reciver + " : " + message + ' : from : ' + socket.username);
        if (reciver === 'all') {
            io.sockets.emit('new_message', { message: message, username: socket.username });
        } else {
            if (!users[reciver])
                io.to(socket.id).emit('new_message', { message: 'user is not registered', username: reciver });
            else if (users[reciver].connected)
                io.to(users[reciver].id).emit('new_message', { message: message, username: socket.username });
            else
                io.to(socket.id).emit('new_message', { message: 'user is not online', username: reciver });
        }

    })

})
